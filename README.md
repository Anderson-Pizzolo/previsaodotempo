# API de Mudanças Climáticas: Gestão de Dados Cruciais

Este projeto visa criar uma API para gerenciar dados relacionados às mudanças climáticas, permitindo o armazenamento, consulta, atualização e remoção de informações sobre fenômenos climáticos, países afetados e relatórios detalhados. A API foi desenvolvida utilizando o framework Spring Boot e pode ser utilizada para monitorar e analisar o impacto das mudanças climáticas em diferentes regiões do mundo.Além disso, utiliza a API OpenWeatherMap para obter informações sobre o tempo atual.

## Sumário

- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Pré-requisitos](#pré-requisitos)
- [Instalação](#instalação)
- [Uso](#uso)
- [Endpoints](#endpoints)

## Tecnologias Utilizadas

- Java 17
- Spring Boot
- Spring Data JPA
- H2 database
- lombok
- Maven


## Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:

- Git
- Java 17
- Maven

## Instalação

### Clonando o Repositório

```bash
git clone https://github.com/seu-usuario/projeto-mudancas-climaticas.git
cd projeto-mudancas-climaticas
```
### Configurando a API OpenWeatherMap

- Crie uma conta no OpenWeatherMap e obtenha sua chave de API.
- Abra o arquivo WeatherService.java localizado em src/main/java/com/clima/PrevisaoDoTempo/service/WeatherService.java.
Substitua o valor da constante API_KEY pela sua chave de API:
java
```
private final String API_KEY = "sua_chave_de_api_aqui";
```
## Uso

```
http://localhost:8080
```
## Endpoints
### clima
- GET /clima: Retorna todos os fenômenos climáticos.
- GET /clima/{id}: Retorna um fenômeno climático específico por ID.
- POST /clima: Cria um novo fenômeno climático.
- PUT /clima/{id}: Atualiza um fenômeno climático existente por ID.
- DELETE /clima/{id}: Exclui um fenômeno climático por ID.
### pais
- GET /pais: Retorna todos os países afetados.
- GET /pais/{id}: Retorna um país específico por ID.
- POST /pais: Adiciona um novo país afetado.
- PUT /pais/{id}: Atualiza um país afetado existente por ID.
- DELETE /pais/{id}: Exclui um país afetado por ID.
### reports 
- GET /reports: Retorna todos os relatórios detalhados.
- GET /reports/{id}: Retorna um relatório específico por ID.
- POST /reports: Cria um novo relatório.
- PUT /reports/{id}: Atualiza um relatório existente por ID.
- DELETE /reports/{id}: Exclui um relatório por ID.

### Acesso ao Console do H2
O console do H2 Database está disponível em http://localhost:8080/h2-console. Use as credenciais configuradas no arquivo `application.yml` para acessar o console.

- JDBC URL: jdbc:h2:mem:mydb
- User Name: sa
- Password: 1234

