package com.clima.PrevisaoDoTempo.repositorio;

import com.clima.PrevisaoDoTempo.models.Clima;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClimaRepository extends JpaRepository<Clima, Long> {
}
