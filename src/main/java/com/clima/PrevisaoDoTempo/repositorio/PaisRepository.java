package com.clima.PrevisaoDoTempo.repositorio;

import com.clima.PrevisaoDoTempo.models.Pais;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Pais, Long> {
}
