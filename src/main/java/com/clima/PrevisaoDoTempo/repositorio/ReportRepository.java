package com.clima.PrevisaoDoTempo.repositorio;

import com.clima.PrevisaoDoTempo.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
