package com.clima.PrevisaoDoTempo.controller;

import com.clima.PrevisaoDoTempo.models.Pais;
import com.clima.PrevisaoDoTempo.services.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pais")
public class PaisController {
    @Autowired
    private PaisService service;

    @PostMapping
    public Pais create(@RequestBody Pais Pais) {
        return service.save(Pais);
    }

    @GetMapping
    public List<Pais> getAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Pais getById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PutMapping("/{id}")
    public Pais update(@PathVariable Long id, @RequestBody Pais Pais) {
        Pais existing = service.findById(id);
        if (existing != null) {
            Pais.setId(id);
            return service.save(Pais);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}