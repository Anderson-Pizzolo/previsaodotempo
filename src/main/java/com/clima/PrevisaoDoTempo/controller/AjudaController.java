package com.clima.PrevisaoDoTempo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ajuda")
public class AjudaController {
    @GetMapping

    public String getAjuda() {
        return "{\n" +
                "  \"estudantes\": [\"Anderson Pizzolo\",  \"Gabriel Kaufmann\"],\n" +
                "  \"projeto\": \"Clima Hoje\",\n" +
                "  \"tema\": \"Mudanças Climáticas\"\n" +
                "}";
    }
}

