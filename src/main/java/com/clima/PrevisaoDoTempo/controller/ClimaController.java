package com.clima.PrevisaoDoTempo.controller;

import com.clima.PrevisaoDoTempo.models.Clima;
import com.clima.PrevisaoDoTempo.services.ClimaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clima")
public class ClimaController {
    @Autowired
    private ClimaService service;

    @PostMapping
    public Clima creat (@RequestBody Clima clima){
        return service.save(clima);
    }
    @GetMapping
    public List<Clima> getAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Clima getById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PutMapping("/{id}")
    public Clima update(@PathVariable Long id, @RequestBody Clima Clima) {
        Clima existing = service.findById(id);
        if (existing != null) {
            Clima.setId(id);
            return service.save(Clima);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}
