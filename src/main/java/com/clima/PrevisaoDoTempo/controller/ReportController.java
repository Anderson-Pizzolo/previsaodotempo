package com.clima.PrevisaoDoTempo.controller;

import com.clima.PrevisaoDoTempo.models.Report;
import com.clima.PrevisaoDoTempo.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController {
    @Autowired
    private ReportService service;

    @PostMapping
    public Report create(@RequestBody Report report) {
        return service.save(report);
    }

    @GetMapping
    public List<Report> getAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Report getById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PutMapping("/{id}")
    public Report update(@PathVariable Long id, @RequestBody Report report) {
        Report existing = service.findById(id);
        if (existing != null) {
            report.setId(id);
            return service.save(report);
        }
        return null;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        service.deleteById(id);
    }
}