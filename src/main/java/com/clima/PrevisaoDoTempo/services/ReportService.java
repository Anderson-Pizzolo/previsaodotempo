package com.clima.PrevisaoDoTempo.services;

import com.clima.PrevisaoDoTempo.models.Report;
import com.clima.PrevisaoDoTempo.repositorio.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {
    @Autowired
    private ReportRepository repository;

    public Report save(Report report) {
        return repository.save(report);
    }

    public List<Report> findAll() {
        return repository.findAll();
    }

    public Report findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

}
