package com.clima.PrevisaoDoTempo.services;

import com.clima.PrevisaoDoTempo.models.Clima;
import com.clima.PrevisaoDoTempo.repositorio.ClimaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClimaService {
    @Autowired
    private ClimaRepository repository;

    public Clima save(Clima clima) {
        return repository.save(clima);
    }

    public List<Clima> findAll() {
        return repository.findAll();
    }

    public Clima findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
