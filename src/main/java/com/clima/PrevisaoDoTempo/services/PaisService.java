package com.clima.PrevisaoDoTempo.services;

import com.clima.PrevisaoDoTempo.models.Pais;
import com.clima.PrevisaoDoTempo.repositorio.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaisService {
    @Autowired
    private PaisRepository repository;
// Método para salvar um objeto 'Pais' no repositório.
    public Pais save(Pais country) {
        return repository.save(country);
    }

// Método para recuperar todos os objetos 'Pais' do repositório.
    public List<Pais> findAll() {
        return repository.findAll();
    }

    // Método para encontrar um objeto 'Pais' pelo seu ID.
    public Pais findById(Long id) {
        return repository.findById(id).orElse(null);
    }

// Método para deletar um objeto 'Pais' pelo seu ID.
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

}

